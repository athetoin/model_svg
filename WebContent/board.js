/**
 * 
 */
$(window).on("load", function() {
	$("#crtPckg").on("click", calc.addPckg);
	$(window).on("resize", calc.redo);
	calc.redo();
});

var calc = function() {
	var ex;
	var mdlHght;
	var mdlWdth;
	var smblWdth;
	var smblHght;
	const qBrd = "#model>ul";
	const qPckg = qBrd + ">li";
	const svgNS = "http://www.w3.org/2000/svg";

	function getCntr() {
		$("#pckCnt").html(
			$(qPckg).length
		);
	}

	function redo() {
		mdlHght = $("#board").height();
		mdlWdth = $("#board").width();
		ex = $("#unit").width();
		smblWdth = $("#package")[0].viewBox.baseVal.width;
		smblHght = $("#package")[0].viewBox.baseVal.height;
		var pckgHght;
		var pckgWdth;
		if(mdlHght / smblHght > mdlWdth / smblWdth){
			pckgWdth = mdlWdth;
			pckgHght = smblHght * pckgWdth / smblWdth;
		} else {
			pckgHght = mdlHght;
			pckgWdth = smblWdth * pckgHght / smblHght;
		}

		$("#mdlDim").html(mdlWdth + " X " + mdlHght);

		var pcksCnt = $(qPckg).length;
		pckgHght = mdlHght / pcksCnt;
		$(qPckg).each(function(i, e) {
			$(e).width(pckgWdth - .4 * ex);
			$(e).height(pckgHght - .4 * ex);
		})
	}

	function addPckg() {
		var sctn = $("<li>");
		var svg = $(document.createElementNS(svgNS, "svg"));
		var use = $(document.createElementNS(svgNS, "use"))
			.addClass("package")
			.attr({
				href: "#package",
				id: "pckg" + $(qPckg).length + 1
			});
		$(qBrd).append(sctn.append(svg.append(use)));
		getCntr();
		redo();
	}
	return {
		redo: redo,
		addPckg: addPckg
	};

}();
